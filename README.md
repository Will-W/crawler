# Web Crawler

## Usage

The build Requires nightly rust due to use of async/await preview functionality.
One known good build (with working `rls`) is
`nightly-2019-05-22-x86_64-unknown-linux-gnu`.

To use this version, run:

`rustup default nightly-2019-05-22`

Alternatively, just install the toolchain and use it on demand:

* `rustup install nightly-2019-05-22`
* Then add `+nightly-2019-05-22` immediately after `cargo` in all the following commands.

### Running natively

Run via cargo:

`cargo run --bin crawlerweb`.

This runs on port `7878`, which is shown in the log output.

### Running via docker

Running `docker build -t crawler .` will build the docker image.

The image can then be started with:

`docker run -p 7878:7878 --rm crawler:latest`

This will bind port 7878 on the host, giving the same effect as running the
binary natively.

### Usage

 The main endpoint is `/crawlsync` which takes one parameter 'url'. The url
should be a fully qualified domain, and will need to be urlencoded as per usual
for a GET parameter.

For example, in a separate console run:

`curl -v http://127.0.0.1:7878/crawlsync?url=https%3A%2F%2Fwww.rust-lang.org%2F`

This will produce JSON output.

### Command line interface

There is also a command line executable for convenient testing.

Can be run in the normal way via cargo. `--help` is implemented, but there is only one argument:

`cargo run --bin crawlercmd https://www.rust-lang.org/`

### Tests
* In one shell: `cd testserver; python3 server.py`
* In another: `cargo test`

See below for detail of testing approach

## Code organisation

Most of the code is in a lib crate, with the work of the code done in two main places:

* `retrieve.rs` which works on single pages, downloading and parsing links
* `coordinator.rs` performs the logic of walking the tree of links, trying to avoid visiting pages multiple times

These are linked together in the `lib.rs` to provide the one main exported
function.

* `main_cmd.rs` provides the command line implementation.
* `main_web.rs` provides the web interface (using Gotham)

## Parallelism

Multiple URLs are retrieved in parallel using tokio. The tokio preview of
async/await is used, which uses macros to provide the funcationality. This
made for clean(ish) code in the main coordinator function, but results in some
future conversion back and forth between old and new types.

In addition to the need to faff with types, this was also a horrific nightmare of
compiler versions, crate versions, differing future versions, lack of
documentation and a general unfinished moving target of interfaces.

For an actual production version against stable rust, more standard future
combinators would do the same thing, but with harder to understand code.
Alternatively, a more conventional threadpool (using e.g. Rayon) could be used.
A controlling thread receives results, and dispatches associated jobs to the
threadpool. Each job would perform the request, then put the result on an mpsc
queue back to the original thread.

## Testing

The retrieval code has a couple of unit tests for internals, but is mostly integration tested against a dummy webserver. For simplicity this is done using the out of the box python webserver against some static files (which therefore means we can't directly test redirects).

The co-ordinator testing stubs out the retrieval and are tested fully 'offline'.

## Future extensions

### Handling of non-HTML

This implemention only retrieves HTML page (sets `Accept` to `text/html,
application/xhtml+xml, application/xml`). This is done to avoid downloading
large binary blobs, and because only HTML pages can have links.

This means that any links to non-html data (e.g. PDF) while show as invalid as
content-negotiation will fail. Depending on the purpose of the crawler, some
alternative handling of the non-HTML content may be necessary.

### robots.txt

There is no implementation of parsing robots.txt and ignoring paths as per the spec. Without this, you'll get junk on some sites.

### Non-blocking request

Currently the crawling is done synchronously with the GET request. This is fine
for smaller crawls, but for larger calls is limiting for the client, and may
result in timeouts.

The simplest RESTful interface would be:

* `POST` the crawl request information to `/crawls/new`
* This kicks off the crawl in the background, returns a `202 Accepted`, and sets
the location header of the response to (e.g.) `/crawls/1234`
* The client polls `/crawls/1234` using `GET` periodically
* While the crawl is in progress, this returns `202`
* Once the crawl is complete, this returns `200` and the crawl data as a JSON
  body

An alternative implementation has a separate `/crawls_pending/1234` endpoint to
query the status when ongoing, and that eventually provides a location header to
the actual data when complete.

Internally, the server would maintain a map of crawlid to crawl result, holding
all the data for all crawls, pending and completed. In a long running production
service, you would also need to implement expiry of old results to prevent ever
growing memory usage.
