FROM rustlang/rust:nightly

WORKDIR /usr/src/crawler
COPY . .

RUN cargo install --bin crawlerweb --path .

EXPOSE 7878

CMD ["crawlerweb"]
