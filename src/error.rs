#[derive(Debug)]
pub struct CrawlerError {
    source: String,
    message: String,
}

impl From<reqwest::Error> for CrawlerError {
    fn from(error: reqwest::Error) -> Self {
        CrawlerError {
            source: String::from("reqwest"),
            message: String::from(error.to_string()),
        }
    }
}

impl From<&str> for CrawlerError {
    fn from(error: &str) -> Self {
        CrawlerError {
            source: String::from("retriever"),
            message: String::from(error),
        }
    }
}

impl From<std::string::FromUtf8Error> for CrawlerError {
    fn from(error: std::string::FromUtf8Error) -> Self {
        CrawlerError {
            source: String::from("page decode"),
            message: error.to_string(),
        }
    }
}
