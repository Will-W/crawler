use std::future::Future;
use tokio::runtime::current_thread::Runtime;

use crate::compat_backward::Compat;

pub fn run_async<O, E>(fut: impl Future<Output = Result<O, E>>) -> Result<O, E> {
    let mut rt = Runtime::new().unwrap();

    rt.block_on(Compat::new(fut))
}
