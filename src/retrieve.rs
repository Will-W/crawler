use regex::Regex;
use reqwest::r#async::Client;
use reqwest::{header, Method, Url};
use log::info;

use crate::compat_forward::IntoAwaitable;
use crate::error::*;
use lazy_static::lazy_static;
use std::collections::HashSet;
use tokio::prelude::*;

/// Gets text body from URL
///
/// This function is tested by the integration testing only
async fn get_body(url: Url) -> Result<(Url, String), CrawlerError> {
    let client = Client::new();
    let response = await!(client
        .request(Method::GET, url)
        .header(
            header::ACCEPT,
            "text/html, application/xhtml+xml, application/xml",
        )
        .send()
        .into_awaitable())?;

    let actual_loc = response.url().clone();

    if !response.status().is_success() {
        return Err(CrawlerError::from("404"));
    }

    let body = response.into_body();
    let chunk = await!(body.concat2().into_awaitable())?;
    let full_text = String::from_utf8(Vec::from(chunk.as_ref()))?;

    Ok((actual_loc, full_text))
}

/// Checks is url is relative
///
/// Based on a simple regex for a scheme followed by a colon.
/// This is actually a pretty passable implemention of rfc1808.
fn is_relative(v: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[a-zA-Z]+:").unwrap();
    }

    !RE.is_match(v)
}

/// Extract all relative URLs from HTML provided as string
fn parse_relative_links(text: &str) -> HashSet<String> {
    let doc = select::document::Document::from(text);

    doc.find(select::predicate::Name("a"))
        .filter_map(|n| n.attr("href"))
        .filter(|s| is_relative(s)) // Select only relative links
        .map(|s| String::from(s))
        .collect()
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct PageInfo {
    pub links: HashSet<String>,
    pub actual_loc: Url,
}

pub async fn links_from_page(url: Url) -> Result<PageInfo, CrawlerError> {
    info!("Checking {}", url);
    let (actual_loc, body) = await!(get_body(url))?;

    Ok(PageInfo {
        links: parse_relative_links(&body),
        actual_loc, // HACK - while we're not returning URLs
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_links() {
        let mut ref_value = HashSet::new();
        ref_value.insert(String::from("/target"));
        let input = "<html><head><meta charset=\"utf-8\"><meta name=\"author\" content=\"\"></head><body><p>Some text with a <a href=\"/target\">link</a></p></body></html>";

        assert_eq!(parse_relative_links(input), ref_value);
    }

    #[test]
    fn test_ignore_email() {
        let input = "<html><body><p>Mail me at <a href=\"mailto:team@usedoorstep.com\">team@usedoorstep.com</a></p></body></html>";
        let res = parse_relative_links(input);
        assert!(res.is_empty());
    }

    #[test]
    fn test_ignore_absolute() {
        let input = "<html><body><p>See our friends at <a href=\"https://somewhereelse.com\">Somewhere else</a></p></body></html>";
        let res = parse_relative_links(input);
        assert!(res.is_empty());
    }

    #[test]
    fn test_is_relative() {
        assert!(!is_relative("https://hello.com"));
        assert!(!is_relative("http://hello.com"));
        assert!(!is_relative("ftp://hello.com"));
        assert!(!is_relative("mailto:thing@otherthing.com"));
        assert!(is_relative("page"));
        assert!(is_relative("/page"));
    }
}
