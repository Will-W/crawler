//! A web endpoint for the crawler
//! Supports graceful shutdown on Ctrl+C.

use futures::{Future, Stream};
use hyper::StatusCode;
use serde_derive::{Serialize, Deserialize};
use log::info;

use gotham::handler::{HandlerFuture, IntoHandlerError};
use gotham::helpers::http::response::create_response;
use gotham::router::builder::DefineSingleRoute;
use gotham::router::builder::{build_simple_router, DrawRoutes};
use gotham::router::Router;
use gotham::state::{FromState, State};
use gotham_derive::{StateData, StaticResponseExtender};

use crawlerlib::compat_backward::Compat;

#[derive(Deserialize, StateData, StaticResponseExtender)]
struct CrawlRequestParams {
    url: String,
}

#[derive(Serialize)]
struct CrawlResponse {
    results: crawlerlib::CrawlData,
    count_valid: usize
}

impl CrawlResponse {
    fn new(data: crawlerlib::CrawlData) -> CrawlResponse {
        let count_valid = data.iter()
                .filter(|(_, p)| match p {
                    crawlerlib::PageResult::Valid(_) => true,
                    _ => false,
                })
                .count();

        CrawlResponse {
            results: data,
            count_valid
        }
    }
}

fn router() -> Router {
    build_simple_router(|route| {
        route
            .get("/crawlsync")
            .with_query_string_extractor::<CrawlRequestParams>()
            .to(crawlsync);
        ;
    })
}

fn crawlsync(mut state: State) -> Box<HandlerFuture> {
    let url_string = CrawlRequestParams::take_from(&mut state).url;
    info!("Received request for sync: {}", url_string);

    let url = url_string.parse::<reqwest::Url>().unwrap(); //TODO - error handling

    let crawl_result = Compat::new(crawlerlib::walk_pages(url));
    Box::new(crawl_result.then(|result| match result {
        Ok(crawl_data) => {
            let response = CrawlResponse::new(crawl_data);
            let data = serde_json::to_vec_pretty(&response).unwrap();
            let res = create_response(&state, StatusCode::OK, mime::APPLICATION_JSON, data);
            Ok((state, res))
        }
        Err(_e) => {
            // Seems to be the easiest way to build an arbitrary HandlerError
            // (copied from docs)
            let io_error = std::io::Error::last_os_error();
            Err((state, io_error.into_handler_error()))
        }
    }))
}

/// Start a server and call the `Handler` we've defined above for each `Request` we receive.
pub fn main() {
    simple_logger::init_with_level(log::Level::Info).unwrap();
    let addr = "0.0.0.0:7878";

    let server = gotham::init_server(addr, router());

    // Future to wait for Ctrl+C.
    let signal = tokio_signal::ctrl_c()
        .flatten_stream()
        .map_err(|error| panic!("Error listening for signal: {}", error))
        .take(1)
        .for_each(|()| {
            println!("Ctrl+C pressed");
            Ok(())
        });

    let serve_until = signal
        .select(server)
        .map(|(res, _)| res)
        .map_err(|(error, _)| error);

    tokio::run(serve_until);

    println!("Shutting down gracefully");
}
