#![feature(await_macro, async_await)]
#![feature(arbitrary_self_types)]

pub mod compat_backward;
pub mod compat_forward;
mod coordinator;
mod error;
mod retrieve;
pub use crate::coordinator::{CrawlData, PageResult};
pub use crate::retrieve::*;
pub mod test_util;

use crate::coordinator::*;
use reqwest::Url;

struct RealRetriever {}
impl Retriever for RealRetriever {
    fn links_from_page(&mut self, url: Url) -> RetrieverFuture {
        Box::pin(crate::retrieve::links_from_page(url.clone()))
    }
}
pub async fn walk_pages(root_page: Url) -> Result<CrawlData, ()> {
    let mut retriever = RealRetriever {};
    await!(crate::coordinator::walk_pages(root_page, &mut retriever))
}
