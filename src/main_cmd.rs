extern crate clap;
use clap::{App, Arg};
use crawlerlib::compat_backward::Compat;
use crawlerlib::PageResult;
use reqwest::Url;
use tokio::runtime::current_thread::Runtime;

fn main() {
    let matches = App::new("Web crawler")
        .author("Will Wykeham (will@wykeham.net)")
        .arg(
            Arg::with_name("START")
                .help("Fully qualified URL of starting point for crawling")
                .required(true)
                .index(1),
        )
        .get_matches();

    let start = matches.value_of("START").unwrap();
    match start.parse::<Url>() {
        Err(_) => {
            println!("Invalid URL. Please provide fully qualified URL.");
        }
        Ok(url) => {
            let mut runtime = Runtime::new().unwrap();
            let fut = crawlerlib::walk_pages(url);
            let pages = runtime.block_on(Compat::new(fut)).unwrap();
            for (s, result) in &pages {
                match result {
                    PageResult::Valid(p) => println!("{: <10}{}", "OK", p.as_str()),
                    PageResult::Invalid => println!("{: <10}{}", "BAD", s),
                    PageResult::Redirect(to) => {
                        println!("{: <10}{} -> {}", "REDIRECT", s, to.as_str())
                    }
                    PageResult::InProgress => (),
                }
            }

            let count = pages
                .iter()
                .filter(|(_, p)| match p {
                    PageResult::Valid(_) => true,
                    _ => false,
                })
                .count();
            println!(
                "\n Total pages (excluding invalid and redirects): {}",
                count
            );
        }
    }
}
