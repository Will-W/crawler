use serde::ser::{Serialize, SerializeStruct, Serializer};
use std::collections::HashMap;
use std::pin::Pin;
use tokio::prelude::*;
use log::info;

use reqwest::Url;

use crate::compat_backward::Compat;
use crate::compat_forward::IntoAwaitable;

use crate::error::CrawlerError;
use crate::retrieve::PageInfo;

pub type RetrieverFuture =
    Pin<Box<std::future::Future<Output = Result<PageInfo, CrawlerError>> + Send>>;

/// Trait for the mechanism of actually retrieving pages
pub trait Retriever {
    fn links_from_page(&mut self, url: Url) -> RetrieverFuture;
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub enum PageResult {
    Valid(Url),
    Invalid,
    Redirect(Url),
    InProgress,
}

impl Serialize for PageResult {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("result", 2)?;
        match self {
            PageResult::Valid(url) => {
                state.serialize_field("status", "ok")?;
                state.serialize_field("url", url.as_str())?;
            }
            PageResult::Invalid => {
                state.serialize_field("status", "not found")?;
                state.serialize_field("url", "")?;
            }
            PageResult::Redirect(url) => {
                state.serialize_field("status", "redirect")?;
                state.serialize_field("url", url.as_str())?;
            }
            PageResult::InProgress => {
                state.serialize_field("status", "not completed")?;
                state.serialize_field("url", "")?;
            }
        }
        state.end()
    }
}

/// Bind the URL we're requesting into the result
///
/// We need the URL we asked for when processing the result. We store that in
/// the closures below and provide it when the future is resolved (either ok or
/// err) so that it's avaialble for recording errors or checking for redirects
fn async_retrieve(
    retriever: &mut impl Retriever,
    url: &Url,
) -> impl futures::Future<Item = (Url, PageInfo), Error = (Url, CrawlerError)> + Send {
    let url_ok = url.clone();
    let url_err = url.clone();
    Compat::new(retriever.links_from_page(url.clone()))
        .map(move |res| (url_ok, res))
        .map_err(move |res| (url_err, res))
}

pub type CrawlData = HashMap<String, PageResult>;

pub async fn walk_pages(
    root_page: Url,
    retriever: &mut impl Retriever,
) -> Result<CrawlData, ()> {
    let mut results: CrawlData = HashMap::new();
    let mut in_progress = vec![async_retrieve(retriever, &root_page)];

    while !in_progress.is_empty() {
        let pg = await!(future::select_all(in_progress).into_awaitable());

        match pg {
            Ok(((request_url, page_info), _, rest)) => {
                let mut remaining_in_progress = rest;

                // Record this page we were visiting as good
                results.insert(
                    String::from(page_info.actual_loc.as_str()),
                    PageResult::Valid(page_info.actual_loc.clone()),
                );
                info!("Found: {}", request_url);

                // If the before and after URLs didn't match then there was a redirect
                if page_info.actual_loc != request_url {
                    results.insert(
                        String::from(request_url.as_str()),
                        PageResult::Redirect(page_info.actual_loc.clone()),
                    );
                }

                // Separate out links into valid and invalid
                let join_base = page_info.actual_loc;
                let joined_links = page_info.links.into_iter().map(|link_string| {
                    match join_base.join(&link_string) {
                        Ok(url) => Ok(url),
                        Err(_) => Err(link_string),
                    }
                });

                for link_result in joined_links {
                    match link_result {
                        Ok(url) => {
                            let mut url = url.clone();
                            url.set_fragment(None);

                            if results.get(url.as_str()).is_none() {
                                info!("Queueing: {}", url);

                                // Add the future to the list to service
                                remaining_in_progress.push(async_retrieve(retriever, &url));

                                // Also add an 'inprogress' entry to the results
                                // to prevent duplicate queries
                                // This entry will get updated when the final
                                // results come back
                                results.insert(String::from(url.as_str()), PageResult::InProgress);
                            }
                        }
                        Err(linkstring) => {
                            results.insert(linkstring, PageResult::Invalid);
                            ()
                        }
                    }
                }
                in_progress = remaining_in_progress;
            }

            Err(((request_url, _error), _, rest)) => {
                results.insert(String::from(request_url.as_str()), PageResult::Invalid);
                in_progress = rest;
            }
        }
    }

    Ok(results)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::compat_forward::IntoAwaitable;
    use crate::test_util::*;
    use std::collections::HashSet;

    /// Implementation of the 'Retriever' trait producing results against static data
    struct TestRetriever {
        pages: HashMap<String, PageInfo>,
    }

    impl TestRetriever {
        fn new(pages: Vec<(String, PageInfo)>) -> TestRetriever {
            TestRetriever {
                pages: pages.into_iter().collect(),
            }
        }

        fn links(&mut self, url: Url) -> Result<PageInfo, CrawlerError> {
            let page = self.pages.get(url.as_str()).ok_or("404")?;
            Ok(page.clone())
        }
    }

    impl Retriever for TestRetriever {
        fn links_from_page(&mut self, url: Url) -> RetrieverFuture {
            Box::pin(future::result(self.links(url)).into_awaitable())
        }
    }

    impl crate::retrieve::PageInfo {
        // A useful convenience method so that the reference data in tests
        // isn't *too* horrific
        pub fn new(url_str: &str, link_strings: &[&str]) -> PageInfo {
            let links: HashSet<String> =
                link_strings.into_iter().map(|s| String::from(*s)).collect();

            let actual_loc = url_str.parse::<Url>().unwrap();

            PageInfo { links, actual_loc }
        }
    }

    fn make_valid(s: &str) -> (String, PageResult) {
        (
            String::from(s),
            PageResult::Valid(s.parse::<Url>().unwrap()),
        )
    }

    fn make_redirect(from: &str, to: &str) -> (String, PageResult) {
        (
            String::from(from),
            PageResult::Redirect(to.parse::<Url>().unwrap()),
        )
    }

    #[test]
    fn find_all_pages() {
        let mut retriever = TestRetriever::new(vec![
            // List of pages. Format is:
            // (
            //     String::from("address that this info will be returned for"),
            //     PageInfo::new(
            //         "Actual address of data (after redirects)",
            //         &[list of urls found on page],
            //     ),
            // ),
            (
                String::from("http://127.0.0.1:4000/"),
                PageInfo::new(
                    "http://127.0.0.1:4000/index.html",
                    &["http://127.0.0.1:4000/a", "http://127.0.0.1:4000/b"],
                ),
            ),
            (
                String::from("http://127.0.0.1:4000/index.html"),
                PageInfo::new(
                    "http://127.0.0.1:4000/index.html",
                    &["http://127.0.0.1:4000/a", "http://127.0.0.1:4000/b"],
                ),
            ),
            (
                String::from("http://127.0.0.1:4000/a"),
                PageInfo::new("http://127.0.0.1:4000/a", &["http://127.0.0.1:4000/c"]),
            ),
            (
                String::from("http://127.0.0.1:4000/b"),
                PageInfo::new("http://127.0.0.1:4000/b", &[]),
            ),
            (
                String::from("http://127.0.0.1:4000/c"),
                PageInfo::new("http://127.0.0.1:4000/c", &["http://127.0.0.1:4000/d"]),
            ),
        ]);

        let ref_value: HashMap<String, PageResult> = vec![
            make_valid("http://127.0.0.1:4000/index.html"),
            make_valid("http://127.0.0.1:4000/a"),
            make_valid("http://127.0.0.1:4000/b"),
            make_valid("http://127.0.0.1:4000/c"),
            make_redirect("http://127.0.0.1:4000/", "http://127.0.0.1:4000/index.html"),
            (String::from("http://127.0.0.1:4000/d"), PageResult::Invalid),
        ]
        .into_iter()
        .collect();

        let res = run_async(walk_pages(
            "http://127.0.0.1:4000/".parse::<Url>().unwrap(),
            &mut retriever,
        ))
        .unwrap();

        assert_eq!(res, ref_value);
    }
}
