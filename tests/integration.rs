use crawlerlib::test_util::*;
use crawlerlib::*;
use reqwest::Url;

#[test]
fn get_links_from_valid_page() {
    let ref_value: std::collections::HashSet<String> = [
        "/home",
        "/registration/new",
        "/session/new",
        "/privacy_policy",
        "/terms_and_conditions",
        "/privacy_policy",
        "/terms_and_conditions",
    ]
    .iter()
    .map(|s| String::from(*s))
    .collect();

    let url = "http://127.0.0.1:4000".parse::<Url>().unwrap();
    let result = run_async(links_from_page(url)).unwrap();
    assert_eq!(result.links, ref_value);
}

#[test]
fn get_error_from_invalid_page() {
    let url = "http://127.0.0.1:4000/no_page_here".parse::<Url>().unwrap();

    assert!(run_async(links_from_page(url)).is_err());
}
